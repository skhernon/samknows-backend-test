# Sam Knows Backend Technical Test Submission

Provided solution to [Sam Knows Backend Technical test](https://github.com/SamKnows/backend-test)

## Instructions

### Prerequisites

The following dependencies are required for the project to run

```
PHP > 7.0
Composer
An instance of Mysql Server
A text editor
```

### Installing dependencies

From the root directory of the project, run

```
composer install
```

### Running tests

After installing dependencies, from the root directory of the project, run

```
vendor/bin/phpunit
```

### Configuring the application

Copy the file `config/config-dist.json` to `config/config.json` and set the necessary parameters

The database user must have write access and be able to create tables in the specified schema, and insert into them.

### Running the application

From the root of the project, run `php ./app.php`.

By default, the application will process the json found at http://tech-test.sandbox.samknows.com/php-2.0/testdata.json,
but this can be overridden by passing a uri as such `php ./app.php --source 'location-of-the-json'`

## Architectural information

The source raw data is loaded into a "Processor" inside the DataProcessingCommand.

A stream is created from the source and parsed by a streaming json processor. The idea is that we don't want
to load everything in memory at once, in case the JSON is large, so the processor fires a series of events
when certain tokens are found in the JSON and these are handled by a listener.

The listener recognises points in the json and builds the hourly aggregates, which are then passed to a writer.
The writer generates a table name aggregated_data_{timestamp}, where the aggregated data for this run is written to.

The table has the following structure -

```
CREATE TABLE `aggregated_data` (
    `unit_id` BIGINT,
    `metric` ENUM('download', 'upload', 'latency', 'packet_loss'),
    `hour` TINYINT,
    `mean` DOUBLE,
    `minimum` DOUBLE,
    `maximum` DOUBLE,
    `median` DOUBLE,
    `sample_size` BIGINT,
    PRIMARY KEY (`unit_id`, `metric`, `hour`)
);
```

To query for a given unit, metric and hour's aggregated data, we can query by
unit_id, metric and hour (remembering the table name is actually aggregated_data_{timestamp}), where {timestamp}
was the unix timestamp when it was generated.

## Improvements to be made

### Logging

Exceptions are not currently logged anywhere, but it would be helpful to catch them
at the top-level of the application and log them somewhere, for debugging or informational
purposes.

### Use Big Integer library for total

When pre-aggregating the data, a running total is kept in the class PreAggregate before
the aggregation occurs. It's possible that for large values or large data sets, the maximum
integer value could be reached (also possible for the database layer). It could be desirable
to use a big integer library, or split values over several variables / columns in a clever
way, to avoid any overflow issues.

### Use a sorted list structure for storing samples

At the moment, when pre-aggregating, the values for the hour are stored in an array and
this is sorted at the end, before the aggregation takes place. This is to make it possible
to find the median for that hour. A list which is kept sorted as values are added may produce faster
execution times, rather than sorting everything at the end.

### Config for the database

Instead of using a simple json file for the application, there may be a library which can
be used to store the configuration in a way that is easier to manage, or a solution that is
more secure (no plain text db password stored on application server).

### Discard outliers from median calculation

At the moment, all values are considered when calculating the median, even if they lie
far outside the first to third quartiles

### Validate JSON

The listener that is parsing and aggregating data points from the JSON is quite
a naive implementation in that it expects the data to be well formed as in the
example and does not validate it. Some validation which handles poorly-formed
inputs some nice way (logging, fixing the data etc) could be something to consider.

### Output progress

The command currently outputs no progress of the process so far, so it's not
possible to see how close we are to being finished.

### Bulk insert

The aggregate writer currently inserts one row at a time. Buffering the data
and inserting multiple would help reduce the effect of latency and protocol overhead.

### Integration tests with docker

The command and the SQL layer currently have to be tested manually. The instructions
say don't use docker but it would still be an improvement if we did, since we could
automate testing the edges of the application.

### Unit test for value objects

This one is debatable. Some people argue that it's pointless to test getters, but
I have seen scenarios before where missing return statements caused errors (though
it has been argued that if this doesn't cause other tests to fail then either the
value objects are pointless, or the system is lacking testing in other, more key areas).

### Rollback on failure

It may be desirable to roll back the data we have already inserted, if the process
fails part-way through. This depends on the needs of the business.

### PHPDoc

Good documentation inside the code itself can be invaluable for helping people
unfamiliar with the code to learn the author's intentions. Tests are great help
as well though, and more guaranteed to be correct (if they pass then they should be).

### Output table name

Currently the only way to find the table name that was generated by the run,
is to run `SHOW TABLES` or query the `INFORMATION_SCHEMA` database. Outputting
this on the console would make querying the data easier.

### Move table name generation out of the writer

The writer is responsible for both generating the table name and inserting the
aggregated data. Isolating the table name generation elsewhere would be more
flexible and make the writer more focused.