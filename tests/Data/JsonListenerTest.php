<?php

namespace SamKnows\BackendTest\Data;

use JsonStreamingParser\Parser;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use SamKnows\BackendTest\Aggregate\Aggregate;
use SamKnows\BackendTest\Aggregate\AggregateIdentifier;
use SamKnows\BackendTest\Aggregate\IdentifiedAggregate;

class JsonListenerTest extends MockeryTestCase
{
    /**
     * @var Mockery\MockInterface|\SamKnows\BackendTest\Data\Writer
     */
    private $writer;

    /**
     * @var JsonListener
     */
    private $listener;

    protected function setUp()
    {
        $this->writer = $this->writer();
        $this->listener = new JsonListener($this->writer);
    }

    /**
     * @return Mockery\MockInterface|\SamKnows\BackendTest\Aggregate\Writer
     */
    private function writer()
    {
        $writer = Mockery::mock('SamKnows\BackendTest\Aggregate\Writer');

        $writtenItems = [];

        $writer->shouldReceive("write")->andReturnUsing(
            function (IdentifiedAggregate $aggregate) use (&$writtenItems) {
                $writtenItems []= $aggregate;
            }
        );

        $writer->shouldReceive("writtenItems")->andReturnUsing(
            function () use (&$writtenItems) {
                return $writtenItems;
            }
        );

        return $writer;
    }

    public function testIsAbleToParseAndWriteDataCorrectly()
    {
        $data = [
            [
                "unit_id" => 1,
                "metrics" => [
                    "download" => [
                        [
                            "timestamp" => "2018-03-07 20:00:00",
                            "value" => 4670170
                        ],
                        [
                            "timestamp" => "2018-03-08 16:00:00",
                            "value" => 4670570
                        ],
                        [
                            "timestamp" => "2018-03-04 16:00:00",
                            "value" => 4670500
                        ],
                        [
                            "timestamp" => "2018-03-05 16:00:00",
                            "value" => 4670600
                        ]
                    ],
                    "upload" => [
                        [
                            "timestamp" => "2018-03-07 13:00:00",
                            "value" => 1214720
                        ]
                    ],
                    "latency" => [
                        [
                            "timestamp" => "2018-03-07 09:00:00",
                            "value" => 44868
                        ]
                    ],
                    "packet_loss" => [
                        [
                            "timestamp" => "2018-03-07 01:00:00",
                            "value" => 0.12
                        ],
                        [
                            "timestamp" => "2018-03-08 15:00:00",
                            "value" => 0.17
                        ],
                        [
                            "timestamp" => "2018-03-09 15:00:00",
                            "value" => 0.17
                        ],
                        [
                            "timestamp" => "2018-03-10 15:00:00",
                            "value" => 0.86
                        ],
                        [
                            "timestamp" => "2018-03-11 15:00:00",
                            "value" => 0.14
                        ]
                    ]
                ]
            ],
            [
                "unit_id" => 2,
                "metrics" => [
                    "download" => [
                        [
                            "timestamp" => "2018-03-07 20:00:00",
                            "value" => 6670170
                        ]
                    ],
                    "upload" => [
                        [
                            "timestamp" => "2018-03-07 13:00:00",
                            "value" => 2214720
                        ],
                        [
                            "timestamp" => "2018-04-07 13:00:00",
                            "value" => 2216720
                        ],
                        [
                            "timestamp" => "2018-04-07 13:00:00",
                            "value" => 2216700
                        ],
                        [
                            "timestamp" => "2018-04-07 13:00:00",
                            "value" => 2213720
                        ]
                    ],
                    "latency" => [
                        [
                            "timestamp" => "2018-03-07 09:00:00",
                            "value" => 54868
                        ]
                    ],
                    "packet_loss" => [
                        [
                            "timestamp" => "2018-03-07 00:00:00",
                            "value" => 0.12
                        ]
                    ]
                ]
            ]
        ];

        $stream = fopen('data://text/plain,' . json_encode($data), 'r');
        $parser = new Parser($stream, $this->listener);
        $parser->parse();
        fclose($stream);

        $this->assertEquals(
            [
                new IdentifiedAggregate(
                    new AggregateIdentifier(1, "download", 20),
                    new Aggregate(4670170, 4670170, 4670170, 4670170, 1)
                ),
                new IdentifiedAggregate(
                    new AggregateIdentifier(1, "download", 16),
                    new Aggregate(
                        (4670570 + 4670500 + 4670600) / 3,
                        4670600,
                        4670500,
                        4670570,
                        3
                    )
                ),
                new IdentifiedAggregate(
                    new AggregateIdentifier(1, "upload", 13),
                    new Aggregate(1214720, 1214720, 1214720, 1214720, 1)
                ),
                new IdentifiedAggregate(
                    new AggregateIdentifier(1, "latency", 9),
                    new Aggregate(44868, 44868, 44868, 44868, 1)
                ),
                new IdentifiedAggregate(
                    new AggregateIdentifier(1, "packet_loss", 1),
                    new Aggregate(0.12, 0.12, 0.12, 0.12, 1)
                ),
                new IdentifiedAggregate(
                    new AggregateIdentifier(1, "packet_loss", 15),
                    new Aggregate(
                        (0.17 + 0.17 + 0.86 + 0.14) / 4,
                        0.86,
                        0.14,
                        0.17,
                        4
                    )
                ),
                new IdentifiedAggregate(
                    new AggregateIdentifier(2, "download", 20),
                    new Aggregate(6670170, 6670170, 6670170, 6670170, 1)
                ),
                new IdentifiedAggregate(
                    new AggregateIdentifier(2, "upload", 13),
                    new Aggregate(
                        (2214720 + 2216720 + 2216700 + 2213720) / 4,
                        2216720,
                        2213720,
                        (2214720 + 2216700) / 2,
                        4
                    )
                ),
                new IdentifiedAggregate(
                    new AggregateIdentifier(2, "latency", 9),
                    new Aggregate(54868, 54868, 54868, 54868, 1)
                ),
                new IdentifiedAggregate(
                    new AggregateIdentifier(2, "packet_loss", 24),
                    new Aggregate(0.12, 0.12, 0.12, 0.12, 1)
                )
            ],
            $this->writer->writtenItems()
        );
    }
}
