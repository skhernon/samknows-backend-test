<?php

namespace SamKnows\BackendTest\Data;

use Mockery;
use Mockery\Adapter\Phpunit\MockeryTestCase;

class JsonListenerFactoryTest extends MockeryTestCase
{
    public function testReturnsListenerThatUsesInjectedWriter()
    {
        /**
         * @var \SamKnows\BackendTest\Aggregate\Writer|Mockery\MockInterface
         */
        $writer = Mockery::mock('SamKnows\BackendTest\Aggregate\Writer');

        $factory = new JsonListenerFactory($writer);
        $listener = $factory->create();

        $this->assertEquals(new JsonListener($writer), $listener);
    }
}