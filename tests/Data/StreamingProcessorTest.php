<?php

namespace SamKnows\BackendTest\Data;

use Exception;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use SamKnows\BackendTest\Aggregate\Aggregate;
use SamKnows\BackendTest\Aggregate\AggregateIdentifier;
use SamKnows\BackendTest\Aggregate\IdentifiedAggregate;
use SamKnows\BackendTest\Aggregate\Writer;

class StreamingProcessorTest extends MockeryTestCase
{
    /**
     * @var Writer|Mockery\MockInterface
     */
    private $writer;

    /**
     * @var JsonListenerFactory
     */
    private $listenerFactory;

    /**
     * @var StreamingProcessor
     */
    private $processor;

    protected function setUp()
    {
        $this->writer = Mockery::mock('SamKnows\BackendTest\Aggregate\Writer');
        $this->listenerFactory = new JsonListenerFactory($this->writer);
        $this->processor = new StreamingProcessor($this->listenerFactory);
    }

    public function testParsesData()
    {
        $jsonData = json_encode(
            [
                [
                    "unit_id" => 1,
                    "metrics" => [
                        "download" => [
                            [
                                "timestamp" => "2018-03-07 20:00:00",
                                "value" => 4670170
                            ]
                        ]
                    ]
                ]
            ]
        );

        $this->writer->shouldReceive("write")->once()->with(
            Mockery::on(
                function (IdentifiedAggregate $givenAggregate) {
                    return $givenAggregate == new IdentifiedAggregate(
                        new AggregateIdentifier(1, "download", 20),
                        new Aggregate(4670170, 4670170, 4670170, 4670170, 1)
                    );
                }
            )
        );

        $this->processor->process(new Source("data://text/plain,$jsonData"));
    }

    /**
     * @expectedException \SamKnows\BackendTest\Data\ProcessingException
     * @expectedExceptionMessage Unable to open data source with uri: banana
     */
    public function testThrowsExceptionWhenCannotOpenUri()
    {
        $this->processor->process(new Source("banana"));
    }

    public function testThrowsExceptionWhenProblemProcessingJson()
    {
        $writeException = new Exception();

        $jsonData = json_encode(
            [
                [
                    "unit_id" => 1,
                    "metrics" => [
                        "download" => [
                            [
                                "timestamp" => "2018-03-07 20:00:00",
                                "value" => 4670170
                            ]
                        ]
                    ]
                ]
            ]
        );

        $this->writer->shouldReceive("write")->andThrowExceptions(
            [$writeException]
        );

        $uri = "data://text/plain,$jsonData";

        try {
            $this->processor->process(new Source($uri));
        } catch (ProcessingException $e) {
            $this->assertSame(
                "Error processing json from data source uri: $uri",
                $e->getMessage()
            );

            $this->assertSame($writeException, $e->getPrevious());

            return;
        }

        throw new Exception(
            "Expected exception was never thrown when processing json data"
        );
    }
}
