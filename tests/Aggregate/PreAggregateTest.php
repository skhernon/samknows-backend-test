<?php

namespace SamKnows\BackendTest\Aggregate;

use PHPUnit\Framework\TestCase;

class PreAggregateTest extends TestCase
{
    public function testAggregatesValuesCorrectly()
    {
        $values = [7, 42, 1, 78, 9, 3, 13];

        $preAggregate = new PreAggregate();

        foreach ($values as $value) {
            $preAggregate->update($value);
        }

        $aggregate = $preAggregate->finish();

        $this->assertSame(
            array_sum($values) / count($values),
            $aggregate->mean()
        );

        $this->assertSame(min($values), $aggregate->minimum());
        $this->assertSame(max($values),$aggregate->maximum());
        $this->assertSame(9, $aggregate->median());
        $this->assertSame(count($values), $aggregate->sampleSize());
    }

    public function testProducesCorrectMedianForEvenLengthDataSets()
    {
        $values = [7, 42, 1, 78, 9, 3, 13, 12];

        $preAggregate = new PreAggregate();

        foreach ($values as $value) {
            $preAggregate->update($value);
        }

        $aggregate = $preAggregate->finish();

        $this->assertSame((9 + 12) / 2, $aggregate->median());
    }
}
