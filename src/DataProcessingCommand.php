<?php

namespace SamKnows\BackendTest;

use SamKnows\BackendTest\Data\Processor;
use SamKnows\BackendTest\Data\Source;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DataProcessingCommand extends Command
{
    const DEFAULT_DATA_SOURCE
        = "http://tech-test.sandbox.samknows.com/php-2.0/testdata.json";

    /**
     * @var Processor
     */
    private $processor;

    public function __construct(Processor $processor)
    {
        parent::__construct();
        $this->processor = $processor;
    }

    protected function configure()
    {
        $this->setName("process-data")
            ->setDescription("Process and aggregate data units.")
            ->addOption(
                "source",
                "s",
                InputOption::VALUE_REQUIRED,
                "JSON file containing the data to be processed.",
                self::DEFAULT_DATA_SOURCE
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->processor->process(new Source($input->getOption("source")));
    }
}
