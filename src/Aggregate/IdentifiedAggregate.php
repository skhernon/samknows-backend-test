<?php

namespace SamKnows\BackendTest\Aggregate;

final class IdentifiedAggregate
{
    /**
     * @var AggregateIdentifier
     */
    private $identifier;

    /**
     * @var Aggregate
     */
    private $aggregate;


    public function __construct(
        AggregateIdentifier $identifier,
        Aggregate $aggregate
    ) {
        $this->identifier = $identifier;
        $this->aggregate = $aggregate;
    }

    /**
     * @return AggregateIdentifier
     */
    public function identifier()
    {
        return $this->identifier;
    }

    /**
     * @return Aggregate
     */
    public function aggregate()
    {
        return $this->aggregate;
    }
}
