<?php

namespace SamKnows\BackendTest\Aggregate;

final class AggregateIdentifier
{
    /**
     * @var int
     */
    private $unitId;

    /**
     * @var string
     */
    private $metric;

    /**
     * @var int
     */
    private $hour;

    public function __construct($unitId, $metric, $hour)
    {
        $this->unitId = (int) $unitId;
        $this->metric = (string) $metric;
        $this->hour = (int) $hour;
    }

    /**
     * @return int
     */
    public function unitId()
    {
        return $this->unitId;
    }

    /**
     * @return string
     */
    public function metric()
    {
        return $this->metric;
    }

    /**
     * @return int
     */
    public function hour()
    {
        return $this->hour;
    }
}
