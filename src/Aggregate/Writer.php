<?php

namespace SamKnows\BackendTest\Aggregate;

interface Writer
{
    public function write(IdentifiedAggregate $identifiedAggregate);
}
