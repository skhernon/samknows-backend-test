<?php

namespace SamKnows\BackendTest\Aggregate;

class PreAggregate
{
    /**
     * @var int|double
     */
    private $total = 0;

    /**
     * @var int[]|double[]
     */
    private $values = [];

    /**
     * @var int|double
     */
    private $minimum = INF;

    /**
     * @var int|double
     */
    private $maximum = 0;

    /**
     * @var int
     */
    private $sampleSize = 0;

    public function update($value)
    {
        $this->total += $value;
        $this->values []= $value;
        ++$this->sampleSize;

        if ($value > $this->maximum) {
            $this->maximum = $value;
        }

        if ($value < $this->minimum) {
            $this->minimum = $value;
        }
    }

    public function finish() : Aggregate
    {
        sort($this->values, SORT_NUMERIC);

        if ($this->sampleSize % 2 == 1) {
            $median = $this->values[(($this->sampleSize + 1) / 2) - 1];
        } else {
            $upper = $this->values[(($this->sampleSize + 2) / 2) - 1];
            $lower = $this->values[($this->sampleSize / 2) - 1];
            $median = ($upper + $lower) / 2;
        }

        return new Aggregate(
            $this->total / $this->sampleSize,
            $this->maximum,
            $this->minimum,
            $median,
            $this->sampleSize
        );
    }
}
