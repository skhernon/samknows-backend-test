<?php

namespace SamKnows\BackendTest\Aggregate;

final class Aggregate
{
    /**
     * @var double|int
     */
    private $mean;

    /**
     * @var double|int
     */
    private $maximum;

    /**
     * @var double|int
     */
    private $minimum;

    /**
     * @var double|int
     */
    private $median;

    /**
     * @var int
     */
    private $sampleSize;

    public function __construct(
        $mean,
        $maximum,
        $minimum,
        $median,
        $sampleSize
    ) {
        $this->mean = $mean;
        $this->maximum = $maximum;
        $this->minimum = $minimum;
        $this->median = $median;
        $this->sampleSize = (int) $sampleSize;
    }

    /**
     * @return float|int
     */
    public function mean()
    {
        return $this->mean;
    }

    /**
     * @return float|int
     */
    public function maximum()
    {
        return $this->maximum;
    }

    /**
     * @return float|int
     */
    public function minimum()
    {
        return $this->minimum;
    }

    /**
     * @return float|int
     */
    public function median()
    {
        return $this->median;
    }

    /**
     * @return int
     */
    public function sampleSize() : int
    {
        return $this->sampleSize;
    }
}
