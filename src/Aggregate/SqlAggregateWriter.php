<?php

namespace SamKnows\BackendTest\Aggregate;

use Doctrine\DBAL\Connection;

final class SqlAggregateWriter implements Writer
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var string
     */
    private $tableName;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    private function tableName()
    {
        if (!$this->tableName) {
            $this->tableName = "aggregated_data_" . time();

            $this->connection->executeUpdate(
                "
                    CREATE TABLE `{$this->tableName}`(
                        `unit_id` BIGINT,
                        `metric` ENUM('download', 'upload', 'latency', 'packet_loss'),
                        `hour` TINYINT,
                        `mean` DOUBLE,
                        `minimum` DOUBLE,
                        `maximum` DOUBLE,
                        `median` DOUBLE,
                        `sample_size` BIGINT,
                        PRIMARY KEY (`unit_id`, `metric`, `hour`)
                    );
                "
            );
        }

        return $this->tableName;
    }

    public function write(IdentifiedAggregate $identifiedAggregate)
    {
        $identifier = $identifiedAggregate->identifier();
        $aggregate = $identifiedAggregate->aggregate();
        $tableName = $this->tableName();

        $this->connection->executeUpdate(
            "
                INSERT INTO
                    `$tableName`
                        (
                            `unit_id`,
                            `metric`,
                            `hour`,
                            `mean`,
                            `minimum`,
                            `maximum`,
                            `median`,
                            `sample_size`
                        )
                    VALUES
                        (
                            :unitId,
                            :metric,
                            :hour,
                            :mean,
                            :minimum,
                            :maximum,
                            :median,
                            :sampleSize
                        )
            ",
            array(
                "unitId" => $identifier->unitId(),
                "metric" => $identifier->metric(),
                "hour" => $identifier->hour(),
                "mean" => $aggregate->mean(),
                "minimum" => $aggregate->minimum(),
                "maximum" => $aggregate->maximum(),
                "median" => $aggregate->median(),
                "sampleSize" => $aggregate->sampleSize()
            )
        );
    }
}
