<?php

namespace SamKnows\BackendTest\Data;

use DateTime;
use JsonStreamingParser\Listener;
use SamKnows\BackendTest\Aggregate\AggregateIdentifier;
use SamKnows\BackendTest\Aggregate\IdentifiedAggregate;
use SamKnows\BackendTest\Aggregate\PreAggregate;
use SamKnows\BackendTest\Aggregate\Writer;

class JsonListener implements Listener
{
    /**
     * @var PreAggregate[]
     */
    private $preAggregates;

    /**
     * @var Writer
     */
    private $writer;

    /**
     * @var string
     */
    private $currentKey;

    /**
     * @var int
     */
    private $unitId;

    /**
     * @var string
     */
    private $metricName;

    /**
     * @var string
     */
    private $timeStamp;

    /**
     * @var int|double
     */
    private $value;

    /**
     * @var bool
     */
    private $added = false;

    public function __construct(Writer $writer)
    {
        $this->writer = $writer;
    }

    public function startDocument()
    {
        $this->preAggregates = [];
    }

    public function endDocument()
    {
        foreach ($this->preAggregates as $hour => $preAggregate) {
            $this->writer->write(
                new IdentifiedAggregate(
                    new AggregateIdentifier(
                        $this->unitId,
                        $this->metricName,
                        $hour
                    ),
                    $preAggregate->finish()
                )
            );
        }
    }

    public function startObject()
    {
        $this->added = false;
    }

    public function endObject()
    {
        if ($this->added) {
            return;
        }

        switch ($this->currentKey) {
            case "timestamp":
            case "value":
                $dateTime = new DateTime($this->timeStamp);
                $hour = (int) $dateTime->format("G");

                if ($hour == 0) {
                    $hour = 24;
                }

                if (!isset($this->preAggregates[$hour])) {
                    $this->preAggregates[$hour] = new PreAggregate();
                }

                $this->preAggregates[$hour]->update($this->value);
                $this->added = true;
        }
    }

    public function startArray()
    {
    }

    public function endArray()
    {
    }

    public function key($key)
    {
        $this->currentKey = $key;

        switch ($this->currentKey) {
            case MetricNames::DOWNLOAD:
            case MetricNames::UPLOAD:
            case MetricNames::LATENCY:
            case MetricNames::PACKET_LOSS:
                $this->endDocument();
                $this->startDocument();
                $this->metricName = $key;
        }
    }

    public function value($value)
    {
        switch ($this->currentKey) {
            case "unit_id":
                $this->endDocument();
                $this->startDocument();
                $this->unitId = $value;
                break;

            case "timestamp":
                $this->timeStamp = $value;
                break;

            case "value":
                $this->value = $value;
        }
    }

    public function whitespace($whitespace)
    {
    }
}
