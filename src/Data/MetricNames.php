<?php

namespace SamKnows\BackendTest\Data;

final class MetricNames
{
    const DOWNLOAD = "download";
    const LATENCY = "latency";
    const PACKET_LOSS = "packet_loss";
    const UPLOAD = "upload";
}
