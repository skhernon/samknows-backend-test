<?php

namespace SamKnows\BackendTest\Data;

use JsonStreamingParser\Listener;
use SamKnows\BackendTest\Aggregate\Writer;

class JsonListenerFactory
{
    /**
     * @var Writer
     */
    private $writer;

    public function __construct(Writer $writer)
    {
        $this->writer = $writer;
    }

    /**
     * @return Listener
     */
    public function create()
    {
        return new JsonListener($this->writer);
    }
}
