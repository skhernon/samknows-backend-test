<?php

namespace SamKnows\BackendTest\Data;

final class Source
{
    /**
     * @var string
     */
    private $uri;

    public function __construct(string $uri)
    {
        $this->uri = $uri;
    }

    public function uri() : string
    {
        return $this->uri;
    }
}
