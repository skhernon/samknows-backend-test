<?php

namespace SamKnows\BackendTest\Data;

interface Processor
{
    public function process(Source $source);
}
