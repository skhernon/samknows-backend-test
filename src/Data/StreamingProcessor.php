<?php

namespace SamKnows\BackendTest\Data;

use Exception;
use JsonStreamingParser\Parser;

final class StreamingProcessor implements Processor
{
    /**
     * @var JsonListenerFactory
     */
    private $jsonListenerFactory;

    public function __construct(JsonListenerFactory $jsonListenerFactory)
    {
        $this->jsonListenerFactory = $jsonListenerFactory;
    }

    public function process(Source $source)
    {
        $uri = $source->uri();
        $stream = @fopen($uri, 'r');

        if (!$stream) {
            throw new ProcessingException(
                "Unable to open data source with uri: $uri"
            );
        }

        try {
            $parser = new Parser($stream, $this->jsonListenerFactory->create());
            $parser->parse();
        } catch (Exception $e) {
            throw new ProcessingException(
                "Error processing json from data source uri: $uri",
                0,
                $e
            );
        } finally {
            fclose($stream);
        }
    }
}
