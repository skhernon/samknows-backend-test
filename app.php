<?php

use Doctrine\DBAL\DriverManager;
use SamKnows\BackendTest\Aggregate\SqlAggregateWriter;
use SamKnows\BackendTest\Data\JsonListenerFactory;
use SamKnows\BackendTest\Data\StreamingProcessor;
use SamKnows\BackendTest\DataProcessingCommand;
use Symfony\Component\Console\Application;

require_once __DIR__ . "/vendor/autoload.php";

$application = new Application("sam-knows-backend");

$config = json_decode(
    file_get_contents(__DIR__ . "/config/config.json")
);

$command = new DataProcessingCommand(
    new StreamingProcessor(
        new JsonListenerFactory(
            new SqlAggregateWriter(
                DriverManager::getConnection(
                    array(
                        'user' => @$config->database->username,
                        'password' => @$config->database->password,
                        'host' => @$config->database->host,
                        'port' => @$config->database->port,
                        'dbname' => $config->database->database,
                        'driver' => 'pdo_mysql',
                    )
                )
            )
        )
    )
);

$application->add($command);
$application->setDefaultCommand($command->getName(), true);
$application->run();
